package com.controller;

import com.mypojos.Admin;
import com.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Created with IntelliJ IDEA.
 * User: basshamut
 * Date: 30/06/17
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping("/admin")
    public String showAdmin(Model model, @ModelAttribute("resultado") String resultado) {

        Admin admin = new Admin();
        model.addAttribute("admin", admin);
        model.addAttribute("resultado", resultado);

        return "admin";
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public String handleAdmin(@ModelAttribute("admin") Admin adminForm, RedirectAttributes ra, @RequestParam("estado") String estado){
        System.out.println(adminForm);
        System.out.println(estado);
        ra.addFlashAttribute("resultado", "Cambios realizados con exito");

        return "redirect:/admin";
    }
}
