package com.mypojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by Jesus on 17-04-2017.
 */

@Entity
@Table(name="Admin")
public class Admin {

    @Id
    @GeneratedValue
    @Column(name="idAdmin")
    private int idAdmin;
    @Column(name="nombre")
    private String nombre;
    @Column(name="cargo")
    private String cargo;
    @Column(name="fechaCreacion")
    private Timestamp fechaCreacion;
    @OneToMany(mappedBy="admin")
    private Set<Direccion> direcciones;

    public Admin(){

    }

    public Admin(String nombre, String cargo, Timestamp fechaCreacion) {
        this.nombre = nombre;
        this.cargo = cargo;
        this.fechaCreacion = fechaCreacion;
    }

    public int getIdAdmin() {
        return idAdmin;
    }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Timestamp getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Timestamp fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Set<Direccion> getDirecciones() {
        return direcciones;
    }

    public void setDirecciones(Set<Direccion> direcciones) {
        this.direcciones = direcciones;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "idAdmin=" + idAdmin +
                ", nombre='" + nombre + '\'' +
                ", cargo='" + cargo + '\'' +
                ", fechaCreacion=" + fechaCreacion +
                '}';
    }
}
