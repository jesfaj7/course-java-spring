package com.dao;

import com.mypojos.Usuario;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class UsuarioDaoImpl implements UsuarioDao {
    @Autowired
    private SessionFactory sessionFactory;

    public Session getSession(){
        return this.sessionFactory.getCurrentSession();
    }
    @Override
    public Usuario findByUsername(String usuario) {
        Criteria criteria = getSession().createCriteria(Usuario.class)
                                        .add(Restrictions.eq("usuario", usuario));
        return (Usuario)criteria.uniqueResult();
    }

    @Override
    public void save(Usuario usuario) {
        getSession().save(usuario);
    }

    @Override
    public List<Usuario> findAll() {
        Query query = getSession().createQuery("from Usuario");
        return query.list();
    }
}
