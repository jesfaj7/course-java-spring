package com.controller;

import com.mypojos.Admin;
import com.mypojos.Usuario;
import com.service.AdminService;
import com.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Jesus Fajardo
 * Date: 30/06/17
 * Time: 13:00
 */
@Controller
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping("/usuario")
    public String showForm(Model model){
        model.addAttribute("usuario", new Usuario());
        model.addAttribute("usuarios", usuarioService.findAll());
        return "usuario";
    }

    @RequestMapping("/usuario/save")
    public String register(@ModelAttribute("usuario") Usuario usuario,  Model model){
        usuarioService.save(usuario);
        return "redirect:/usuario";
    }
}
