<%--
  Created by IntelliJ IDEA.
  User: Jesus Fajardo
  Date: 26/10/17
  Time: 12:10 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h2>Menu</h2>

<sec:authorize access="!isAuthenticated()">
    Por favor inicie sesion
</sec:authorize>

<%--Toma todos lo usuarios--%>
<%--<sec:authorize access="isAuthenticated()">--%>
    <%--<sec:authentication property="principal" var="principal"/>--%>
    <%--<c:set var="username" value="${principal}"/>--%>
    <%--Usuario a iniciado sesion como: <c:out value="${username}"/>--%>
    <%--<br>--%>
    <%--<a href="<c:url value='/logout'/> ">Cerrar Sesion</a>--%>
<%--</sec:authorize>--%>

<%--Toma los usuarios marcados para recordar--%>
<sec:authorize access="isRememberMe()">
    Usuario ha iniciado sesión como:
    <sec:authentication property="principal" var="principal"/>
    <c:set var="username" value="${principal.username}"/>
    <c:out value="${username}"></c:out>
    <br/>
    <a href="<c:url value='/logout'/>">Cerrar sesión</a>
</sec:authorize>

<%--Toma los usuarios logeados por primera vez--%>
<sec:authorize access="isFullyAuthenticated()">
    Usuario ha iniciado sesión como:
    <sec:authentication property="principal" var="principal"/>
    <c:set var="username" value="${principal}"/>
    <c:out value="${username}"></c:out>
    <br/>
    <a href="<c:url value='/logout'/>">Cerrar sesión</a>
</sec:authorize>

<a href="<c:url value="/usuario"/>">Registrar Usuario</a>