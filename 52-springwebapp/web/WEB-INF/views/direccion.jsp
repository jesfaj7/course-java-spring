<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <script type="text/javascript" src='<c:url value="/resources/js/jquery.js" />'></script>
    <script type="text/javascript">
    </script>
</head>
<body>
    <c:import url="/WEB-INF/views/menu.jsp"/>
	<h1>Direccion.jsp</h1>
    <sf:form action="${pageContext.request.contextPath}/direccion/save" method="post" commandName="direccion">
        <table>
            <tr>
                <td>Calle</td>
                <td><sf:input path="calle" type="text"/></td>
            </tr>
            <tr>
                <td>C.P.</td>
                <td><sf:input path="cp" type="text"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Guardar cambios"/></td>
            </tr>
        </table>
    </sf:form>
    <br>
    <br>
    <c:out value="${resultado}"/><br>
    <c:if test="${direcciones ne null}">
    <table border="1">
        <thead>
            <tr>
                <td>Calle</td>
                <td>C.P.</td>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${direcciones}" var="direccion">
            <tr>
                <td>
                    <c:out value="${direccion.calle}"/>
                </td>
                <td>
                    <c:out value="${direccion.cp}"/>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </c:if>
</body>
</html>