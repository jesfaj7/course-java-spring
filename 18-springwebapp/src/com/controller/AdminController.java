package com.controller;

import com.mypojos.Admin;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA.
 * User: basshamut
 * Date: 30/06/17
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class AdminController {
    @RequestMapping("/admin")
    public String showAdmin(Model model) {

        Admin admin = new Admin();
        model.addAttribute("admin", admin);

        return "admin";
    }
}
