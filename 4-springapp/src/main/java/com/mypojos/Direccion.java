package com.mypojos;

import org.springframework.stereotype.Component;

/**
 * Created by Jesus on 18-04-2017.
 */

@Component
public class Direccion {
    private String calle;
    private String cp;

    public Direccion() {
    }

    public Direccion(String calle, String cp) {
        this.calle = calle;
        this.cp = cp;
    }

    @Override
    public String toString() {
        return "Direccion{" +
                "calle='" + calle + '\'' +
                ", cp='" + cp + '\'' +
                '}';
    }
}
