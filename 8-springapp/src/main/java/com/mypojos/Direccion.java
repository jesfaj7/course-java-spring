package com.mypojos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Jesus on 18-04-2017.
 */

@Component
public class Direccion {
    private String calle;
    private String cp;

    @Autowired
    public void setCalle(@Value("Principal") String calle) {
        this.calle = calle;
    }

    @Autowired
    public void setCp(@Value("1221")String cp) {
        this.cp = cp;
    }

    public Direccion() {
    }

    public Direccion(String calle, String cp) {
        this.calle = calle;
        this.cp = cp;
    }

    @Override
    public String toString() {
        return "Direccion{" +
                "calle='" + calle + '\'' +
                ", cp='" + cp + '\'' +
                '}';
    }
}
