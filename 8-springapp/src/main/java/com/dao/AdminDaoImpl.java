package com.dao;

import com.mypojos.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * Created by jesus on 31/05/17.
 */
@Component("adminDao")
public class AdminDaoImpl implements AdminDao{
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public boolean save(Admin admin) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
        mapSqlParameterSource.addValue("nombre",admin.getNombre());
        mapSqlParameterSource.addValue("cargo",admin.getCargo());
        mapSqlParameterSource.addValue("fechaCreacion",admin.getFechaCreacion());
        return jdbcTemplate.
                update("insert into Admin(nombre, cargo, fechaCreacion) values(:nombre, :cargo, :fechaCreacion)", mapSqlParameterSource) == 1;
    }
}
