import com.dao.AdminDao;
import com.mypojos.Admin;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Jesus on 17-04-2017.
 */
public class MainApp {
    public static void main (String args[]){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring_config.xml");

        AdminDao adminDao = (AdminDao) applicationContext.getBean("adminDao");
        Admin admin = new Admin();
        Timestamp ts = new Timestamp(new Date().getTime());
        admin.setNombre("Jesus");
        admin.setCargo("Desarrollador");
        admin.setFechaCreacion(ts);
        try{
            adminDao.save(admin);
            System.out.psrintln("Success: Administrador guardado!");
        }catch(CannotGetJdbcConnectionException ex) {
            System.out.println("Error: No se pudo guardar el administrador!");
            ex.printStackTrace();
        }catch(DataAccessException ex){
            System.out.println("Error: No se pudo guardar el administrador!");
            ex.printStackTrace();
        }

        ((ClassPathXmlApplicationContext)applicationContext).close();
    }
}
