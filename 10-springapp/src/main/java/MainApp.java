import com.dao.AdminDao;
import com.mypojos.Admin;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by Jesus on 17-04-2017.
 */
public class MainApp {
    public static void main (String args[]){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring_config.xml");

        AdminDao adminDao = (AdminDao) applicationContext.getBean("adminDao");
        Admin admin = new Admin();
        Timestamp ts = new Timestamp(new Date().getTime());
        admin.setNombre("Johan");
        admin.setCargo("Programador");
        admin.setFechaCreacion(ts);
        try{
            adminDao.save(admin);
            System.out.println("Success: Administrador guardado!");
            System.out.println("All admins: ");
            List<Admin> admins = adminDao.findAll();
            for (Admin adminItem : admins){
                System.out.println(adminItem);
            }
            System.out.println("Admins by id: ");
            System.out.println(adminDao.findById(1));
            System.out.println(adminDao.findById(3));
            System.out.println("Admins by name: ");
            System.out.println(adminDao.findByName("Joh").toString());

        }catch(CannotGetJdbcConnectionException ex) {
            ex.printStackTrace();
        }catch(DataAccessException ex){
            ex.printStackTrace();
        }

        ((ClassPathXmlApplicationContext)applicationContext).close();
    }
}
