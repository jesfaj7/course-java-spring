<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <script type="text/javascript" src='<c:url value="/resources/js/jquery.js" />'></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery(".confirm").on("click", function(){
                return confirm("Si se elimina este elemento no se podra recuperar. Continuar?");
            });
        });
    </script>
</head>
<body>
	<h1>Admin.jsp</h1>
    <sf:form action="${pageContext.request.contextPath}/admin/save" method="post" commandName="admin">
        <%--<input name="estado" type="text"/>--%>
        <table>
            <c:if test="${admin.idAdmin ne 0}">
                <sf:input path="idAdmin" type="hidden"/>
                <sf:input path="fechaCreacion" type="hidden"/>
            </c:if>
            <tr>
                <td>Nombre</td>
                <td><sf:input path="nombre" type="text"/></td>
            </tr>
            <tr>
                <td>Cargo</td>
                <td><sf:input path="cargo" type="text"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Guardar cambios"/></td>
            </tr>
        </table>
    </sf:form>
    <br>
    <br>
    <c:out value="${resultado}"/><br>
    <c:if test="${admins ne null}">
    <table border="1">
        <thead>
            <tr>
                <td>Nombre</td>
                <td>Cargo</td>
                <td>Operacion</td>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${admins}" var="admin">
            <tr>
                <td>
                    <c:out value="${admin.nombre}"/>
                </td>
                <td>
                    <c:out value="${admin.cargo}"/>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <a href="<c:url value="/direccion/${admin.idAdmin}"/>">Direcciones</a>
                            </td>
                            <td>
                                <a href="<c:url value="/admin/${admin.idAdmin}/update"/>">Actualizar</a>
                            </td>
                            <td>
                                <a class="confirm" href="<c:url value="/admin/${admin.idAdmin}/delete"/>">Eliminar</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </c:if>
</body>
</html>