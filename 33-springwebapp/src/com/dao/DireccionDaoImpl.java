package com.dao;

import com.mypojos.Admin;
import com.mypojos.Direccion;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by jesus on 31/05/17.
 */

@Transactional
@Repository
public class DireccionDaoImpl implements DireccionDao{

    @Autowired
    private SessionFactory sessionFactory;

    public Session getSession(){
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public void save(Direccion direccion) {
        getSession().save(direccion);
    }

    @Override
    public List<Direccion> findAll(Admin admin) {
        Criteria criteria = getSession().createCriteria(Direccion.class)
                                        .setFetchMode("admin", FetchMode.JOIN)
                                        .add(Restrictions.eq("admin.idAdmin", admin.getIdAdmin()))
                                        .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

}
