package com.dao;

import com.mypojos.Admin;
import com.mypojos.Direccion;
import java.util.List;

public interface DireccionDao {
    public void save(Direccion direccion);
    public List<Direccion> findAll(Admin admin);
}
