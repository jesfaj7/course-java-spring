package com.controller;

import com.mypojos.Admin;
import com.mypojos.Direccion;
import com.service.AdminService;
import com.service.DireccionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: basshamut
 * Date: 30/06/17
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
@Controller
@SessionAttributes("admin")
public class DireccionController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private DireccionService direccionService;

    @RequestMapping("/direccion/{idAdmin}")
    public String getAll(Model model,
                         @ModelAttribute("resultado") String resultado,
                         @PathVariable("idAdmin") int id){
        Admin admin = adminService.findById(id);
        model.addAttribute("admin", admin);
        model.addAttribute("direccion", new Direccion());
        model.addAttribute("resultado", resultado);
        model.addAttribute("direcciones", direccionService.findAll(id));
        return "direccion";
    }

    @RequestMapping("/direccion/save")
    public String save(Model model, RedirectAttributes ra,
                       @ModelAttribute("direccion") Direccion direccion, @ModelAttribute("admin") Admin admin){
        direccionService.save(admin, direccion);
        ra.addFlashAttribute("resultado", "Cambios realizados con exito");
        return "redirect:/direccion/" + admin.getIdAdmin();
    }
}
