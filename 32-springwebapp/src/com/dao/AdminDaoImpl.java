package com.dao;

import com.mypojos.Admin;
import com.mypojos.AdminRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by jesus on 31/05/17.
 */
public class AdminDaoImpl implements AdminDao{
    @Override
    public void save(Admin admin) {

    }

    @Override
    public List<Admin> findAll() {
        return null;
    }

    @Override
    public Admin findById(int id) {
        return null;
    }

    @Override
    public List<Admin> findByName(String name) {
        return null;
    }

    @Override
    public void update(Admin admin) {

    }

    @Override
    public void delete(int id) {

    }
}
