package com.service;

import com.dao.AdminDao;
import com.dao.DireccionDao;
import com.mypojos.Admin;
import com.mypojos.Direccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
public class DireccionService {

    @Autowired
    private AdminDao adminDao;

    @Autowired
    private DireccionDao direccionDao;

    public void save(Admin admin, Direccion direccion){
        direccion.setAdmin(admin);
        direccionDao.save(direccion);

    }

    public List<Direccion> findAll(int id){
        Admin admin = adminDao.findById(id);
        return direccionDao.findAll(admin);
    }

}
