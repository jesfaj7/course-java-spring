<%--
  Created by IntelliJ IDEA.
  User: Jesus Fajardo
  Date: 26/10/17
  Time: 12:10 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h2>Menu</h2>

<sec:authorize access="!isAuthenticated()">
    Por favor inicie sesion
</sec:authorize>

<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal" var="principal"/>
    <c:set var="username" value="${principal.username}"/>
    Usuario a iniciado sesion como: <c:out value="${username}"/>
    <br>
    <a href="<c:url value='/logout'/> ">Cerrar Sesion</a>
</sec:authorize>