package com.dao;

import com.pojos.Admin;
import java.util.List;

/**
 * Created by jesus on 31/05/17.
 */
public interface AdminDao {
    public void save(Admin admin);
    public List<Admin> findAll();
    public Admin findById(int id);
    public List<Admin> findByName(String name);
    public void update(Admin admin);
    public void delete(Admin admin);
}
