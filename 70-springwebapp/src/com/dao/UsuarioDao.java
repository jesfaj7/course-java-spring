package com.dao;

import com.pojos.Usuario;

import java.util.List;

public interface UsuarioDao {
    public Usuario findByUsername(String usuario);

    void save(Usuario usuario);

    List<Usuario> findAll();
}
