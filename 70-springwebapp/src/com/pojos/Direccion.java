package com.pojos;

import javax.persistence.*;

/**
 * Created by Jesus on 18-04-2017.
 */

@Entity
@Table(name="Direccion")
public class Direccion {

    @Id
    @GeneratedValue
    private int idDir;

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    @Column(name="calle")

    private String calle;
    @Column(name="cp")
    private String cp;
    @ManyToOne
    @JoinColumn(name = "idAdmin")
    private Admin admin;

    public Direccion() {
    }

    public Direccion(String calle, String cp) {
        this.calle = calle;
        this.cp = cp;
    }

    public int getIdDir() {
        return idDir;
    }

    public void setIdDir(int idDir) {
        this.idDir = idDir;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    @Override
    public String toString() {
        return "Direccion{" +
                "idDir=" + idDir +
                ", calle='" + calle + '\'' +
                ", cp='" + cp + '\'' +
                '}';
    }
}
