package com.dao;

import com.mypojos.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by jesus on 31/05/17.
 */
@Component("adminDao")
public class AdminDaoImpl implements AdminDao{
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public boolean save(Admin admin) {
//        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource();
//        mapSqlParameterSource.addValue("nombre",admin.getNombre());
//        mapSqlParameterSource.addValue("cargo",admin.getCargo());
//        mapSqlParameterSource.addValue("fechaCreacion",admin.getFechaCreacion());
        BeanPropertySqlParameterSource beanPropertySqlParameterSource = new BeanPropertySqlParameterSource(admin);
//        return jdbcTemplate.
//                update("insert into Admin(nombre, cargo, fechaCreacion) values(:nombre, :cargo, :fechaCreacion)", mapSqlParameterSource) == 1;
        return jdbcTemplate.
                update("insert into Admin(nombre, cargo, fechaCreacion) values(:nombre, :cargo, :fechaCreacion)", beanPropertySqlParameterSource) == 1;
    }

    public List<Admin> findAll() {
        return jdbcTemplate.query("select * from Admin", new RowMapper<Admin>() {
            public Admin mapRow(ResultSet resultSet, int i) throws SQLException {
                Admin admin = new Admin();
                admin.setCargo(resultSet.getString("cargo"));
                admin.setFechaCreacion(resultSet.getTimestamp("fechaCreacion"));
                admin.setNombre(resultSet.getString("nombre"));
                return admin;
            }
        });
    }
}
