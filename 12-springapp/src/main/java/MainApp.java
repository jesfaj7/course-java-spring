import com.dao.AdminDao;
import com.mypojos.Admin;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Jesus on 17-04-2017.
 */
public class MainApp {
    public static void main (String args[]){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring_config.xml");

        AdminDao adminDao = (AdminDao) applicationContext.getBean("adminDao");
        List<Admin> admins = null;
        Admin admin = null;
        admin = new Admin();
        Timestamp ts = new Timestamp(new Date().getTime());
        admin.setNombre("Fabio");
        admin.setCargo("Consultor Senior");
        admin.setFechaCreacion(ts);
        try{
            adminDao.save(admin);
            System.out.println("Success: Administrador guardado!");
            System.out.println("All admins: ");
            admins = adminDao.findAll();
            for (Admin adminItem : admins){
                System.out.println(adminItem);
            }
            System.out.println("Admins by id: ");
            System.out.println(adminDao.findById(1));
            System.out.println(adminDao.findById(3));
            System.out.println("Admins by name: ");
            System.out.println(adminDao.findByName("Joh").toString());
            System.out.println("Updating  / Deleting Admins by name and id: ");
            admin = adminDao.findByName("Eduardo").get(0);
            System.out.println("Update admin: " + admin);
            admin.setNombre("Eduardo");
            admin.setCargo("Desarrollador Jr.");
            if(adminDao.update(admin)){
                admin = adminDao.findByName("Eduardo").get(0);
                System.out.println("---> Updated admin: " + admin);
            }else{
                admin = adminDao.findByName("Jose").get(0);
                System.out.println("---> Can't Update admin: " + admin);
            }

            if(adminDao.delete(admin.getIdAdmin())){
                System.out.println("---> Eliminado admin: " + admin);
            }else{
                System.out.println("---> No se pudo eliminar admin: " + admin);
            }
            System.out.println("Insert usando lista: ");
            admins = new ArrayList<Admin>();
            admins.add(new Admin("pedro", "jefe de ingenieria", ts));
            admins.add(new Admin("jorge", "subjefe de ingenieria", ts));
            admins.add(new Admin("maria", "representante legal", ts));

            int[] vals = adminDao.saveAll(admins);

            for (int i : vals) {
                System.out.println("filas afectadas para la operacion: " + i);
            }

        }catch(CannotGetJdbcConnectionException ex) {
            ex.printStackTrace();
        }catch(DataAccessException ex){
            ex.printStackTrace();
        }

        ((ClassPathXmlApplicationContext)applicationContext).close();
    }
}
