package com.dao;

import com.mypojos.Admin;

import java.util.List;

/**
 * Created by jesus on 31/05/17.
 */
public interface AdminDao {
    public boolean save(Admin admin);
    public List<Admin> findAll();
    public Admin findById(int id);
    public List<Admin> findByName(String name);
    public boolean update(Admin admin);
    public boolean delete(int id);
    public int[] saveAll(List<Admin> admins);
}
