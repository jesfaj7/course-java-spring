package com.mypojos;

/**
 * Created by Jesus on 17-04-2017.
 */
public class Administrador {
    private int idAdmin;
    private String nombre;
    private Direccion direccion;

    public Administrador() {

    }

    public Administrador(int idAdmin, String nombre) {
        this.idAdmin = idAdmin;
        this.nombre = nombre;
    }

    public void setDireccion(Direccion direccion) { this.direccion = direccion; }

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Administrador{" +
                "idAdmin=" + idAdmin +
                ", nombre='" + nombre + '\'' +
                ", direccion=" + direccion +
                '}';
    }

    public void imprimirDireccion(){
        System.out.println("Jesus 201");
    }
}
