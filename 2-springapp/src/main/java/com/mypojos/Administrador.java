package com.mypojos;

/**
 * Created by Jesus on 17-04-2017.
 */
public class Administrador {
    private int idAdmin;
    private String nombre;

    public void setIdAdmin(int idAdmin) {
        this.idAdmin = idAdmin;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Administrador() {

    }

    public Administrador(int idAdmin, String nombre) {
        this.idAdmin = idAdmin;
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Administrador{" +
                "idAdmin=" + idAdmin +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public void imprimirDireccion(){
        System.out.println("Jesus 201");
    }
}
