import com.dao.AdminDao;
import com.mypojos.Admin;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Jesus on 17-04-2017.
 */
public class MainApp {
    public static void main (String args[]){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring_config.xml");

        AdminDao adminDao = (AdminDao) applicationContext.getBean("adminDao");
        Admin admin = new Admin();
        Timestamp ts = new Timestamp(new Date().getTime());
        admin.setNombre("Jesus");
        admin.setCargo("Desarrollador");
        admin.setFechaCreacion(ts);
        if (adminDao.save(admin)){
            System.out.println("Success: Administrador guardado!");
        }else{
            System.out.println("Error: No se pudo guardar el administrador!");
        }

        ((ClassPathXmlApplicationContext)applicationContext).close();
    }
}
