package com.pojos;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by jesus on 02/06/17.
 */
public class AdminRowMapper implements RowMapper<Admin> {
    public Admin mapRow(ResultSet resultSet, int i) throws SQLException {
        Admin admin = new Admin();
        admin.setIdAdmin(resultSet.getInt("idAdmin"));
        admin.setCargo(resultSet.getString("cargo"));
        admin.setFechaCreacion(resultSet.getTimestamp("fechaCreacion"));
        admin.setNombre(resultSet.getString("nombre"));
        return admin;
    }
}
