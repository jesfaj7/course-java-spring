package com.service;

import com.dao.AdminDao;
import com.pojos.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

@Service
public class AdminService {

    @Autowired
    private AdminDao adminDao;

    public void save(Admin admin){

        admin.setFechaCreacion(new Timestamp(new Date().getTime()));
        adminDao.save(admin);
    }

    public void saveOrUpdate(Admin admin){
        if(admin.getIdAdmin() == 0){// Insert
            admin.setFechaCreacion(new Timestamp(new Date().getTime()));
            adminDao.save(admin);
        }else{// Update
            adminDao.update(admin);
        }
    }

    public List<Admin> findAll(){
        return adminDao.findAll();
    }

    public Admin findById(int id){
        return adminDao.findById(id);
    }

    public void delete(int id) {
        Admin admin = this.findById(id);
        adminDao.delete(admin);
    }
}
