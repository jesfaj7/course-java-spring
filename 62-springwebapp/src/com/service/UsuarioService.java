package com.service;

import com.dao.UsuarioDao;
import com.pojos.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioDao usuarioDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void save(Usuario usuario) {
        usuario.setFechaCreacion(new Timestamp(new Date().getTime()));
        String usrClv = usuario.getClave();
        usuario.setClave(passwordEncoder.encode(usrClv));
        usuarioDao.save(usuario);
    }

    public List<Usuario> findAll() {
        return usuarioDao.findAll();
    }
}
