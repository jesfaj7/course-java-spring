package com.dao;

import com.pojos.Admin;
import com.pojos.Direccion;
import java.util.List;

public interface DireccionDao {
    public void save(Direccion direccion);
    public List<Direccion> findAll(Admin admin);
}
