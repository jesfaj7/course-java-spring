package com.controller;

import com.pojos.Usuario;
import com.pojos.valid.SpringFormGroup;
import com.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

/**
 * Created with IntelliJ IDEA.
 * User: Jesus Fajardo
 * Date: 30/06/17
 * Time: 13:00
 */
@Controller
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping("/usuario")
    public String showForm(Model model){
        model.addAttribute("usuario", new Usuario());
        model.addAttribute("usuarios", usuarioService.findAll());
        return "usuario";
    }

    @RequestMapping("/usuario/save")
    public String register(@ModelAttribute("usuario") @Validated(value = SpringFormGroup.class) Usuario usuario,
                           BindingResult result,
                           Model model){
        if(result.hasErrors()){
            return "usuario";
        }
        usuarioService.save(usuario);
        return "redirect:/usuario";
    }
}
