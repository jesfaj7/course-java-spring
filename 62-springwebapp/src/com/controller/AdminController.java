package com.controller;

import com.pojos.Admin;
import com.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Jesus Fajardo
 * Date: 30/06/17
 * Time: 13:00
 */
@Controller
public class AdminController {

    @Autowired
    private AdminService adminService;

    @RequestMapping("/admin")
    public String showAdmin(Model model, @ModelAttribute("resultado") String resultado) {

        Admin admin = new Admin();
        List<Admin> admins = adminService.findAll();

        model.addAttribute("admin", admin);
        model.addAttribute("resultado", resultado);
        model.addAttribute("admins", admins);

        return "admin";
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public String handleAdmin(@ModelAttribute("admin") Admin adminForm, RedirectAttributes ra){
        adminService.saveOrUpdate(adminForm);
        ra.addFlashAttribute("resultado", "Cambios realizados con exito");
        return "redirect:/admin";
    }

    @RequestMapping("/admin/{idAdmin}/update")
    public String showUpdate(Model model, @PathVariable("idAdmin") int id){
        Admin admin = adminService.findById(id);
        model.addAttribute("admin", admin);
        return "admin";
    }

    @RequestMapping("admin/{idAdmin}/delete")
    public String delete(@PathVariable("idAdmin") int id, RedirectAttributes ra){
        adminService.delete(id);
        ra.addFlashAttribute("resultado", "Cambios realizados con exito");
        return "redirect:/admin";
    }
}
