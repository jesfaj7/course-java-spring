package com.service;

import com.dao.AdminDao;
import com.mypojos.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.sql.Timestamp;

@Service
public class AdminService {

    @Autowired
    private AdminDao adminDao;

    public boolean save(Admin admin){

        admin.setFechaCreacion(new Timestamp(new Date().getTime()));
        return adminDao.save(admin);
    }
}
