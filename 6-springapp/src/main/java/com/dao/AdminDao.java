package com.dao;

import com.mypojos.Admin;

/**
 * Created by jesus on 31/05/17.
 */
public interface AdminDao {
    public boolean save(Admin admin);
}
