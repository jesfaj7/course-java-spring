package com.controller;

import com.mypojos.Admin;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: basshamut
 * Date: 30/06/17
 * Time: 13:00
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class AdminController {
    @RequestMapping("/admin")
    public String showAdmin(Model model) {

        Admin admin = new Admin();
        model.addAttribute("admin", admin);

        return "admin";
    }

    @RequestMapping(value = "/admin/save", method = RequestMethod.POST)
    public String handleAdmin(@ModelAttribute("admin") Admin adminForm, Model model){
        model.addAttribute("adminForm", adminForm);
        return "index";
    }
}
